# Demo CI/CD

## Các bước thực hiện

1. Tạo một repository trên gitlab, có đầy đủ các file cần thiết để test và build project.
2. Tạo file `.gitlab-ci.yml` để cấu hình pipeline.
3. Cấu hình file `.gitlab-ci.yml` như hướng dẫn sau:
   1. Stage: gồm 2 stage
      1. `test`:
         1. Tạo môi trường ảo để chạy các bước test.
         2. Cài đặt các thư viện cần thiết.
         3. Chạy các bước test.
      2. `build`:
         1. Tạo môi trường ảo để build project.
         2. Cài đặt các thư viện cần thiết.
         3. Build project.
         4. Tạo image docker.
         5. Push image docker lên Gitlab registry.
   2. Cấu hình cho các job:
      1. `image`: image docker sẽ được sử dụng để chạy các bước trong job.
         1. Tùy thuộc vào project mà sẽ có các image docker khác nhau. VD: django sẽ sử dụng image python, nodejs sẽ sử dụng image node.
      2. `services`: các service sẽ được sử dụng trong job. VD: postgresql, redis, ...
      3. `stages`: các stage mà job sẽ được thực hiện. 
      4. `before_script`: các bước sẽ được thực hiện trước khi chạy các bước trong job. VD: cài đặt các thư viện cần thiết.
      5. `script`: các bước sẽ được thực hiện trong job.
      6. `rules`: các điều kiện để job được chạy. VD: chỉ chạy job khi có commit lên nhánh main.
4. Commit và push code lên Gitlab.
