from django.http import HttpResponse


def health_check_view(request) -> HttpResponse:
    return HttpResponse('It works!')
