from django.apps import AppConfig
from django.core.management import call_command


class DemoConfig(AppConfig):
    name = 'demo'
    verbose_name = "Demo application"

    def ready(self):
        call_command('migrate')
