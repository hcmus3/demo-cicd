from django.db import models


class Department(models.Model):
    name = models.CharField(max_length=50)
    location = models.CharField(max_length=50)

    class Meta:
        db_table = 'department'

    def __str__(self):
        return self.name


class Employee(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    address = models.CharField(max_length=50)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)

    class Meta:
        db_table = 'employee'

    def __str__(self):
        return self.name


class Salary(models.Model):
    user = models.ForeignKey(Employee, on_delete=models.CASCADE)
    amount = models.IntegerField()
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)

    class Meta:
        db_table = 'salary'

    def __str__(self):
        return self.name
