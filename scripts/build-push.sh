#! /usr/bin/env bash
set -e

docker build -t registry.gitlab.com/hcmus3/demo-cicd:$2 \
  --build-arg CI_JOB_TOKEN=$1 \
  --build-arg BRANCH_NAME=$2 \
  --build-arg RUN_TYPE=$3 \
  .
docker push registry.gitlab.com/hcmus3/demo-cicd:$2
