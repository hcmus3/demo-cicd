from django.test import Client, TestCase
from django.urls import reverse


class TestViews(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_return_health_check_view_properly(self):
        response = self.client.get(reverse('health_check'))
        self.assertEqual(response.status_code, 200)
